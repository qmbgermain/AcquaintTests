﻿Import-Module $PSScriptRoot\shared.psm1 -Force

Given 'WebDriver runs' {
    Ensure-QuamotionRunning
}

Given '(?<operatingSystem>\S*) device is available'{
    param($operatingSystem)

    [object[]] $devices = (Get-Device) | Where-Object {$_.configuration.OperatingSystem -eq $operatingSystem}

    if($devices.Count -ge 1)
    {
        Set-CurrentDevice -deviceId $devices[0].uniqueId
    }
    else
    {
        Throw "No $operatingSystem device available"
    }
}

Given 'Application (?<appId>\S*) exists for (?<operatingSystem>\S*)'{
    param($appId, $operatingSystem)

    [object[]] $apps = (Get-App) | Where-Object {$_.AppId -eq $appId -and $_.SupportedConfigurations.OperatingSystem -eq $operatingSystem}

    if($apps.Count -ge 1)
    {
        Set-CurrentAppId -appId $apps[0].AppId
        Set-CurrentAppVersion -appVersion $apps[0].Version
    }
    else
    {
        Throw "$app not available"
    }
}

Given 'Acquaint is launched'{
    New-Session -deviceId (Get-CurrentDevice) -appId (Get-CurrentAppId) -appVersion (Get-CurrentAppVersion)
    Set-Timeout -time 10000
}

Then 'Close acquaint'{
    Remove-Session
}

Given 'Dataset (?<dataSet>\S*) is used' {
    param($dataSet)
    $setupNeeded = Test-Element -xpath "AppCompatEditText[@marked='setupDataPartitionPhraseField']"
    $setupNeeded | Should -Be $true

    if($setupNeeded)
    {
        Click-Element -xpath "AppCompatEditText[@marked='setupDataPartitionPhraseField']"
        Clear-Text
        Enter-Text $dataSet
        Dismiss-Keyboard

        Click-Element -xpath "AppCompatButton[@marked='setupContinueButton']"
    }
}

When 'Add new contact'{
    param($contactInformation)

    $contact = $contactInformation | ConvertFrom-Json

    Click-Element -xpath "FloatingActionButton[@marked='acquaintanceListFloatingActionButton']"

    Click-Element -xpath "AppCompatEditText[@marked='firstNameField']"
    Clear-Text
    Enter-Text $contact.firstName
    Dismiss-Keyboard

    Click-Element -xpath "AppCompatEditText[@marked='lastNameField']"
    Clear-Text
    Enter-Text $contact.lastName
    Dismiss-Keyboard

    Click-Element -xpath "AppCompatEditText[@marked='companyField']"
    Clear-Text
    Enter-Text $contact.company
    Dismiss-Keyboard

    Click-Element -xpath "AppCompatEditText[@marked='emailField']"
    Clear-Text
    Enter-Text $contact.email
    Dismiss-Keyboard

    Click-Element -xpath "ActionMenuItemView[@marked='acquaintanceSaveButton']"
}

Then 'Contact with name (?<lastName>\S*), (?<firstName>\S*) is added'{
    param($firstName, $lastName)

    $condition = "AppCompatTextView[@marked='$lastName, $firstName']"
    Write-Host $condition
    Test-Element -xpath $condition | Should -Be $true
}