﻿$currentDeviceId = $null
$currentAppId = $null
$currentAppVersion = $null

$defaultTimeout = 60000

function Set-CurrentDevice
{
    param ([Parameter(Mandatory=$true)] [string] $deviceId)

    $script:currentDeviceId = $deviceId
}

function Get-CurrentDevice
{
    return $script:currentDeviceId
}

function Set-CurrentAppId
{
    param ([Parameter(Mandatory=$true)] [string] $appId)

    $script:currentAppId = $appId
}

function Get-CurrentAppId
{
    return $script:currentAppId
}

function Set-CurrentAppVersion
{
    param ([Parameter(Mandatory=$true)] [string] $appVersion)

    $script:currentAppVersion = $appVersion
}

function Get-CurrentAppVersion
{
    return $script:currentAppVersion
}

Function Ensure-QuamotionRunning
{
    param([string] $wdHost = "http://localhost:17894/wd/hub")
    
    Try
    {
        $status = Invoke-WebRequest -Uri "$wdHost/status" -TimeoutSec 5 -ErrorAction Stop
    }
    Catch
    {
        Throw "Quamotion is not running"
    }
}