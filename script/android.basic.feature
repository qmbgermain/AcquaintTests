Feature: Basic scenarios

Background: Initialize
    When WebDriver runs
    And Android device is available
    And Application demo.quamotion.acquaint exists for Android

Scenario: Start Stop
    Given Acquaint is launched
    Then Close acquaint

Scenario: Add Contact
    Given Acquaint is launched
    Given Dataset UseLocalDataSource is used
    When Add new contact
        """
        {
            "firstName":"GenyMotion",
            "lastName":"Android",
            "company":"Genymobile",
            "email":"genymotion@genymobile.com"
        }
        """
    Then Contact with name Android, GenyMotion is added
    Then Close acquaint